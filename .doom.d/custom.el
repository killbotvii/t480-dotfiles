(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("0a41da554c41c9169bdaba5745468608706c9046231bbbc0d155af1a12f32271" default))
 '(org-agenda-files '("~/Org/agenda.org"))
 '(package-selected-packages
   '(org-ql alert writeroom-mode crux expand-region emms-state emms weblorg elfeed lsp-pyright ytdious)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(font-lock-comment-face ((t (:slant italic))))
 '(font-lock-keyword-face ((t (:slant italic)))))
