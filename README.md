![My simple and practical Qtile layout](Qtile-screenshot.png)

Dotfiles for my Thinkpad t480 laptop. This will shortly replace my 80k9 laptop in all use cases. As with the others, this will mainly be a merger between Luke Smith and Distrotube's dotfile sensibilities, skewing towards Luke's.

Note to self: When it comes time to move to a new computer, run "yay -Qet | awk '{print $1}' > packages.txt" to get a full list of packages you installed. Make sure to use that to get everything set up on the new computer. Afterwards, run "yay -S - < packages.txt --needed" (--needed may not work in this context, needs testing)
